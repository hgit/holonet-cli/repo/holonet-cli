<?php
/**
 * This file is part of the holonet cli package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\cli\error;

use RuntimeException;

/**
 * Special exception to be thrown if the user does not use the cli application properly.
 */
class InvalidUsageException extends RuntimeException {
}
