<?php
/**
 * This file is part of the holonet cli package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\cli\io;

use RuntimeException;
use Hexmode\IOMode\IOMode;

/**
 * Input reader class reading from the command line (from the user).
 */
class TerminalInput extends InputDevice {
	/**
	 * @var IOMode $ioMode IOMode instance describing the stdin resource we are wrapping around
	 */
	public IOMode $ioMode;

	/**
	 * @var resource $phpstdin Opened file descriptor to php://stdin
	 */
	private $phpstdin;

	public function __construct() {
		$this->phpstdin = fopen('php://stdin', 'rb');
		$this->ioMode = new IOMode($this->phpstdin);
	}

	/**
	 * return global argv array entries with the script name removed.
	 * @return array with the command line parameters from the call
	 */
	public function getArgv(): array {
		$args = $_SERVER['argv'];
		array_shift($args);

		return $args;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getInput($prompt) {
		echo "{$prompt}: ";

		return trim(fgets($this->phpstdin));
	}

	/**
	 * {@inheritdoc}
	 */
	public function getPipedContent(): string {
		if (!$this->hasPipedContent()) {
			throw new RuntimeException('TerminalInput device has no piped content in it');
		}

		return stream_get_contents($this->phpstdin);
	}

	/**
	 * {@inheritdoc}
	 */
	public function hasPipedContent(): bool {
		return $this->ioMode->isFifo() || $this->ioMode->isReg();
	}
}
