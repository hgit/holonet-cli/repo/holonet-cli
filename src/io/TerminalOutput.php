<?php
/**
 * This file is part of the holonet cli package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\cli\io;

use Codedungeon\PHPCliColors\Color;

/**
 * Output class writing to stdout and stderr of a normal command line application.
 */
class TerminalOutput extends OutputDevice {
	/**
	 * holds an opened file descriptor to php://stderr.
	 * @var resource $phpstderr Opened file descriptor to php://stderr
	 */
	private $phpstderr;

	public function __construct() {
		$this->phpstderr = fopen('php://stderr', 'wb');
	}

	/**
	 * {@inheritdoc}
	 */
	public function writeErr(string $msg, string $colour = null): void {
		if ($colour !== null) {
			$msg = "{$colour}{$msg}".Color::RESET;
		}
		fwrite($this->phpstderr, $msg);
	}

	/**
	 * {@inheritdoc}
	 */
	public function writeOut(string $msg, string $colour = null): void {
		if ($colour !== null) {
			$msg = "{$colour}{$msg}".Color::RESET;
		}
		echo $msg;
	}
}
