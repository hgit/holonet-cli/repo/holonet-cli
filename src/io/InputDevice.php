<?php
/**
 * This file is part of the holonet cli package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\cli\io;

use RuntimeException;
use holonet\cli\argparse\ArgumentParser;
use holonet\cli\argparse\ArgparseDefinition;

/**
 * Implementing child classes implement some kind of input interface
 * for the application to interact with the user.
 */
abstract class InputDevice {
	/**
	 * Instance of a definition object for command line arguments the application expects.
	 */
	private ?ArgparseDefinition $argumentDefinition;

	/**
	 * @var array<string, mixed> $options Cache for the parsed options
	 */
	private array $options;

	/**
	 * @param string $name The name of the given parameter
	 * @return mixed the parsed value for the given name/null if doesn't exist
	 */
	public function getArg(string $name) {
		if ($this->argumentDefinition === null) {
			throw new RuntimeException('Cannot parse arguments without argument definition');
		}

		return $this->options[$name] ?? null;
	}

	/**
	 * convenience function used to get one of a choice, allowing a default
	 * if no default is given, the user must type one of the choices
	 * the choice array should be set like this:
	 * $choices["n"] = "name" //short unique value
	 * prints the prompt first.
	 * @param string $prompt Prompt used to ask the user for input
	 * @param array $choices Array with options as defined above
	 * @param string $default Used to specify a default opttion
	 * @return string the name of the selected option
	 */
	public function getChoice(string $prompt, array $choices, string $default = null): string {
		$prompt .= sprintf(' (%s %s)',
			implode('/', $choices),
			implode(',', array_keys($choices))
		);

		while (true) {
			$in = mb_strtolower($this->getInput($prompt));

			if ($in === '') {
				//the user might have just hit enter
				//just return the default then
				if ($default !== null) {
					return $default;
				}
			} else {
				//check if the user gave the shortcut
				if (isset($choices[$in])) {
					return $choices[$in];
				}

				if (in_array($in, $choices)) {
					return $in;
				}
			}
		}

		return '';
	}

	/**
	 * method called in the child class to read user input
	 * prints the prompt first
	 * returns anything, even an empty string is valid.
	 * @param string $prompt Prompt used to ask the user for input
	 * @return mixed Any input the implementing class can return
	 */
	abstract public function getInput(string $prompt);

	/**
	 * convenience function used to return a non empty value
	 * prints the prompt first
	 * returns a non empty value
	 * uses the child class's getInput() method.
	 * @param string $prompt Prompt used to ask the user for input
	 * @return string with non empty input
	 */
	public function getNonEmptyInput(string $prompt): string {
		do {
			$in = (string)($this->getInput($prompt));
		} while ($in === '');

		return $in;
	}

	/**
	 * @return string with the content that was piped to this InputDevice
	 */
	abstract public function getPipedContent(): string;

	/**
	 * convenience function used to read a boolean
	 * prints the prompt first
	 * returns either true or false.
	 * @param string $prompt Prompt used to ask the user for input
	 * @return bool true or false
	 */
	public function getYesOrNo(string $prompt): bool {
		$yesOrNo = $this->getChoice($prompt, array('y' => 'yes', 'n' => 'no'));

		return in_array($yesOrNo, array('y', 'yes'));
	}

	/**
	 * Check if the contained InputDevice has any content that was piped to the process.
	 * @return bool true or false if we have piped content or not
	 */
	abstract public function hasPipedContent(): bool;

	/**
	 * set the argument input definition
	 * resets the internal parsed options cache.
	 * @param ArgparseDefinition $argdef Argument definition object
	 * @param bool $ignoreInvalid Boolean flag to allow for partial parsing of options
	 */
	public function setArgumentDefinition(ArgparseDefinition $argdef, bool $ignoreInvalid = false): void {
		$this->options = array();
		$this->argumentDefinition = $argdef;
		$raw = $this->getArgv();
		$parser = new ArgumentParser($argdef, $raw);
		$this->options = $parser->parse($ignoreInvalid);
	}

	/**
	 * method called in the child class to get the raw arguments array for that certain implementation.
	 * @return array with raw command line options (might be artificial)
	 */
	abstract protected function getArgv(): array;
}
