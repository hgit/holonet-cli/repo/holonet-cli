<?php
/**
 * This file is part of the holonet cli package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\cli\io;

use Codedungeon\PHPCliColors\Color;

/**
 * Implementing child classes implement some kind of output interface
 * for the command to interact with the user.
 */
abstract class OutputDevice {
	/**
	 * @param string $msg Message to be written to the stderr channel
	 * @param string|null $colour Specify the colour for the output
	 */
	abstract public function writeErr(string $msg, ?string $colour = null): void;

	/**
	 * convenience function used to write a line to stderr
	 * simply appends a newline character before writing.
	 * @param string $msg Message to be written to the stderr channel
	 * @param string|null $colour Specify the colour for the output line
	 */
	public function writeErrLn(string $msg, ?string $colour = Color::RED): void {
		if ($colour === null) {
			$this->writeErr(sprintf('%s%s', $msg, "\n"));
		} else {
			$this->writeErr(sprintf('%s%s%s%s', $colour, $msg, "\n", Color::RESET));
		}
	}

	/**
	 * @param string $msg Message to be written to the stdout channel
	 * @param string|null $colour Specify the colour for the output
	 */
	abstract public function writeOut(string $msg, ?string $colour = null): void;

	/**
	 * convenience function used to write a line to stdout
	 * simply appends a newline character before writing.
	 * @param string $msg Message to be written to the stdout channel
	 * @param string|null $colour Specify the colour for the output line
	 */
	public function writeOutLn(string $msg, ?string $colour = null): void {
		if ($colour === null) {
			//do a reset before writing a line
			$this->writeOut(sprintf('%s%s%s', Color::RESET, $msg, "\n"));
		} else {
			$this->writeOut(sprintf('%s%s%s%s', $colour, $msg, "\n", Color::RESET));
		}
	}
}
