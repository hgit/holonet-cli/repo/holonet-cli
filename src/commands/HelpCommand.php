<?php
/**
 * This file is part of the holonet cli package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\cli\commands;

use holonet\cli\Command;
use holonet\cli\Application;

/**
 * HelpCommand class used to print available sub commands of an application.
 */
class HelpCommand extends Command {
	/**
	 * Reference to the application this help is for.
	 */
	private Application $app;

	public function __construct(Application $app) {
		$this->app = $app;
	}

	public function configure(): void {
	}

	/**
	 * {@inheritdoc}
	 */
	public function describe(): string {
		return 'Display available commands and their usage';
	}

	public function execute(): void {
		$helpoutput = sprintf("%s\nUsage: %s %s\nAvailable commands:\n",
			$this->app->describe(), //application description
			$this->app->scriptName(), //script name
			$this->argumentDefinition->getUsage() //generated help string
		);
		foreach ($this->app->commands as $cmd) {
			$helpoutput .= sprintf(" % -30s%s\n",
				$cmd->name(),
				$cmd->describe()
			);
		}
		$this->output->writeOut($helpoutput);
	}

	/**
	 * {@inheritdoc}
	 */
	public function name(): string {
		return 'help';
	}
}
