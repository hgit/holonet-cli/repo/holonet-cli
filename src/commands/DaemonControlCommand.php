<?php
/**
 * This file is part of the holonet cli abstraction library
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\cli\commands;

use holonet\cli\Command;

/**
 * DaemonControlCommand can be used to start/communicate with a running daemon php process.
 * @psalm-suppress MissingConstructor
 */
class DaemonControlCommand extends Command {
	/**
	 * {@inheritdoc}
	 */
	public function configure(): void {
		$this->argumentDefinition->addArgument('command', 'Command that the daemon should run');
	}

	/**
	 * {@inheritdoc}
	 */
	public function describe(): string {
		return <<<'DESC'
				Use this binary to start/communicate with an already running daemon
				Use it to send control commands that can be one of (start, restart, reload, stop)
			DESC;
	}

	/**
	 * {@inheritdoc}
	 */
	public function execute(): void {
		//@TODO implement communication with the daemon
		throw new \RuntimeException('Not implemented yet');
	}

	/**
	 * {@inheritdoc}
	 */
	public function name(): string {
		return 'daemon control';
	}
}
