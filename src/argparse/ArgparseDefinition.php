<?php
/**
 * This file is part of the holonet cli package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\cli\argparse;

/**
 * The ArgparseDefinition class represents a set of expected command line options/arguments
 * such a definition can be used by the ArgumentParser class to parse arguments.
 */
class ArgparseDefinition {
	/**
	 * @var ArgumentOption[] $options Array with options (arguments where position doesn't matter)
	 */
	public array $options = array();

	/**
	 * @var ArgumentPositional[] $positionals Array with argument definitions where position matters
	 */
	public array $positionals = array();

	/**
	 * convenience adder function for a positional argument.
	 * @param string $name The given name for this argument
	 * @param string $desc Description that should be included in the help output
	 * @param int $nargs Definition of how often this argument should be filled in
	 * @param string|null $metavar Replacement string that should be used in the help output
	 * @return ArgumentPositional object that was newly created for method chaining
	 */
	public function addArgument(string $name, string $desc, ?string $metavar = null, int $nargs = 1): ArgumentPositional {
		$ret = new ArgumentPositional($name, $desc, $metavar, $nargs);
		$this->positionals[] = $ret;

		return $ret;
	}

	/**
	 * convenience adder function for a flag.
	 * @return ArgumentFlag object that was newly created for method chaining
	 */
	public function addFlag($switches, string $name, string $desc, bool $cumulative = false): ArgumentFlag {
		if ($cumulative) {
			$ret = new ArgumentCumulativeFlag($switches, $name, $desc);
		} else {
			$ret = new ArgumentFlag($switches, $name, $desc);
		}
		$this->options[] = $ret;

		return $ret;
	}

	/**
	 * convenience adder function for an option.
	 * @return ArgumentOption object that was newly created for method chaining
	 */
	public function addOption($switches, string $name, string $desc, int $nargs = 1, string $metavar = null): ArgumentOption {
		$ret = new ArgumentOption($switches, $name, $desc, $metavar, $nargs);
		$this->options[] = $ret;

		return $ret;
	}

	/**
	 * method used to find and return a matching options	src\Daemon.php:35:19
	 * returns null if no match can be found.
	 * @param string $token The switch that is used to look for the option
	 * @return ArgumentOption|null either an option that matches the switch or null
	 */
	public function findMatchingOption(string $token): ?ArgumentOption {
		foreach ($this->options as $opt) {
			if ($opt->isMatch($token)) {
				return $opt;
			}
		}

		return null;
	}

	/**
	 * function used to generate a string describing the position of the arguments in the help string
	 * generates something like:
	 *  [-h] [--sum] N [N ...].
	 *
	 *  positional arguments:
	 *   N           an integer for the accumulator
	 *
	 *  optional arguments:
	 *   -h, --help  show this help message and exit
	 *   --sum       sum the integers (default: find the max)
	 *
	 * @param string[] $exclude Array with names of parameters to ignore
	 * @return string describing the usage of the parameters
	 */
	public function getUsage(array $exclude = array()): string {
		$args = '';
		$descropts = '';
		//add the options first
		foreach ($this->options as $opt) {
			if (in_array($opt->name, $exclude)) {
				continue;
			}
			//append something like [--file FILE]
			$args .= sprintf('[%s%s] ',
				$opt->switches[0], //the first switch
				$opt->metavar !== null ? ' '.mb_strtoupper($opt->metavar) : ''//the metavar, if given
			);

			//add an entry to the description
			$descropts .= sprintf(" % -30s%s\n",
				implode(', ', $opt->switches),
				$opt->description
			);
		}

		$descrargs = '';
		//then add the positional arguments
		foreach ($this->positionals as $opt) {
			if (in_array($opt->name, $exclude)) {
				continue;
			}
			if ($opt->nargs !== 0) {
				$args .= mb_strtoupper($opt->metavar).' ';
				if ($opt->nargs !== 1) {
					if ($opt->nargs > 1) {
						//just add the metavar a few times
						for ($i = 0; $i < $opt->nargs - 1; $i++) {
							$args .= mb_strtoupper($opt->metavar).' ';
						}
					} else {
						//add a ... definition
						$args .= '['.mb_strtoupper($opt->metavar).' ...] ';
					}
				}
			}

			//add an entry to the description
			$descrargs .= sprintf(" % -30s%s\n",
				mb_strtoupper($opt->metavar),
				$opt->description
			);
		}

		return sprintf("%s\n\npositional arguments:\n%s\noptional arguments:\n%s",
			$args, $descrargs, $descropts
		);
	}

	/**
	 * small helper function used to store a positional argument value
	 * finds the next unsatisfied positional argument and stores the value.
	 * @param string $token The value that's supposed to be stored
	 */
	public function storeArgumentValue(string $token): void {
		foreach ($this->positionals as $opt) {
			if ($opt->wantMore()) {
				$opt->store($token);

				return;
			}
		}
	}
}
