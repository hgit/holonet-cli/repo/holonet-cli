<?php
/**
 * This file is part of the holonet cli package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\cli\argparse;

/**
 * the ArgumentFlag class represents an argument flag (option with hyphens without a value).
 */
class ArgumentFlag extends ArgumentOption {
	public bool $flag;

	/**
	 * {@inheritdoc}
	 */
	public function __construct($switches, string $name, string $desc) {
		//call the parent class constructor with nargs of 0 (NARGS_NONE)
		parent::__construct($switches, $name, $desc, null, static::NARGS_NONE);
		$this->flag = false;
	}

	/**
	 * {@inheritdoc}
	 */
	public function extract(&$array): void {
		$array[$this->name] = $this->flag;
		$this->flag = false;
	}

	/**
	 * {@inheritdoc}
	 */
	public function store(string $value = ''): void {
		$this->flag = true;
	}
}
