<?php
/**
 * This file is part of the holonet cli package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\cli\argparse;

use LogicException;

/**
 * the ArgumentPositional class represents a positional argument
 * Positional Arguments are defined as having no following value and not being prepended by "-".
 */
class ArgumentPositional extends Argument {
	/**
	 * @param string $name The given name for this argument
	 * @param string $desc Description that should be included in the help output
	 * @param string|null $metavar Replacement string that should be used in the help output
	 * @param int $nargs Definition of how often this argument should be filled in
	 */
	public function __construct(string $name, string $desc, string $metavar = null, int $nargs = 1) {
		if ($nargs === 0) {
			throw new LogicException('positional arguments having no value makes no sense');
		}

		//if no metavar was given, we just use the name
		if ($metavar === null) {
			$metavar = $name;
		}

		parent::__construct($name, $desc, $nargs, $metavar);
	}
}
