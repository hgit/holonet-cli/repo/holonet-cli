<?php
/**
 * This file is part of the holonet cli package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\cli\argparse;

use InvalidArgumentException;

/**
 * the ArgumentOption class represents an argument option (prepended by -/--)
 * can have a value or not.
 */
class ArgumentOption extends Argument {
	/**
	 * @var string[] $switches The switches with prepended (-/--) that can be used to specify this option
	 */
	public array $switches;

	/**
	 * an option is defined as being prepended by -/--.
	 * @throws InvalidArgumentException if an empty switch definition was given
	 */
	public function __construct($switches, string $name, string $desc, string $metavar = null, int $nargs = 1) {
		if (is_string($switches)) {
			$switches = explode('|', $switches);
		} elseif (!is_array($switches)) {
			throw new InvalidArgumentException('The first switches argument for a flag must be an array or a string');
		}

		foreach ($switches as &$switch) {
			$switch = ltrim($switch, '-');
			if (mb_strlen($switch) <= 0) {
				throw new InvalidArgumentException('switch definition cannot be empty');
			}
			if (mb_strlen($switch) === 1) {
				//short option
				$switch = "-{$switch}";
			} else {
				//long option
				$switch = "--{$switch}";
			}
		}
		$this->switches = $switches;

		parent::__construct($name, $desc, $nargs, $metavar);
	}

	/**
	 * @param string $value The given argument that could be a match
	 * @return bool true or false on match or not
	 */
	public function isMatch(string $value): bool {
		//if it isn't an option just return false
		if ($value[0] !== '-') {
			return false;
		}

		return in_array($value, $this->switches);
	}
}
