<?php
/**
 * This file is part of the holonet cli package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\cli\argparse;

/**
 * the static ArgsNormaliser class is used to normalise argv options
 * that includes splitting up key value pairs separated by ":" or "="
 * as well as splitting up cumulative options like -fka.
 */
class ArgsNormaliser {
	/**
	 * Array with all normalised arguments ordered by when they appeared.
	 */
	private array $args = array();

	/**
	 * constructor method used to split up key value pairs with a "=" or ":"
	 *  e.g. -f=test or --directory:test
	 * as well as cumulated options
	 *  e.g. -fgh.
	 * @param array $args The given options that might contain key value pairs
	 */
	private function __construct(array $args) {
		foreach ($args as $token) {
			if ($token[0] !== '-') {
				$this->args[] = $token;
			} else {
				//it's either a short flag (-f) or a long flag (--f)
				if (mb_strpos($token, '=') !== false) {
					$this->explodeKeyVal($token, '=');
				} elseif (mb_strpos($token, ':') !== false) {
					$this->explodeKeyVal($token, ':');
				} else {
					$this->expandOption($token);
				}
			}
		}
	}

	/**
	 * interface method with the outside world
	 * creates an instance of this class in order to normalise arguments.
	 * @param array $args The given options that might contain key value pairs
	 * @return array with normalised argument tokens
	 */
	public static function normalise(array $args): array {
		$me = new static($args);

		return $me->args;
	}

	/**
	 * helper method used to expand cumulative options
	 * this allows for the short options "-f -g -h" be specified like -fgh.
	 * @param string $token The token possibly containing cumulative options
	 */
	private function expandOption(string $token): void {
		if (mb_strpos($token, '--') === 0 || mb_strlen($token) === 2) {
			//long options or short options that are not cumulative cannot be expanded
			$this->args[] = $token;
		} else {
			//cumulative key (e.g. -fgb => -f -g -b)
			foreach (mb_str_split($token) as $char) {
				if ($char !== '-') {
					$this->args[] = "-{$char}";
				}
			}
		}
	}

	/**
	 * helper method used to split up key value pairs with a =/:
	 * since the key part of the split could be a cumulative key too,
	 * the expandOption() method is called on it.
	 * @param string $token The token containing a key value pair
	 * @param string $delimiter The delimiter used to explode the key value pair
	 */
	private function explodeKeyVal(string $token, string $delimiter): void {
		$split = explode($delimiter, ($token));
		//the exploded key could also be cumulative single hyphen options
		//append the possibly expanded option keys first
		$this->expandOption($split[0]);
		//now append our value
		$this->args[] = $split[1];
	}
}
