<?php
/**
 * This file is part of the holonet cli package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\cli\argparse;

/**
 * the ArgumentCumulativeFlag class represents an argument flag (option with hyphens without a value)
 * cumulative means you can specify the same flag multiple times and it will count how many times.
 */
class ArgumentCumulativeFlag extends ArgumentFlag {
	public int $count;

	/**
	 * {@inheritdoc}
	 */
	public function __construct($switches, string $name, string $desc) {
		parent::__construct($switches, $name, $desc);
		$this->count = 0;
	}

	/**
	 * {@inheritdoc}
	 */
	public function extract(&$array): void {
		$array[$this->name] = $this->count;
		$this->count = 0;
	}

	/**
	 * For a cumulative option we can just count how often it was specified.
	 * {@inheritdoc}
	 */
	public function store(string $value = ''): void {
		$this->count++;
	}
}
