<?php
/**
 * This file is part of the holonet cli package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\cli\argparse;

use holonet\cli\error\InvalidUsageException;

/**
 * The ArgumentParser class is used as an object oriented approach to command line argument parsing
 * also, the native getopt function is awful
 * it uses a Definition object as a definition of expected options/parameters.
 */
class ArgumentParser {
	/**
	 * @var array<string, mixed> $args Array with the normalised raw command line options that are to be parsed
	 */
	private array $args;

	private ArgparseDefinition $definition;

	/**
	 * Automatically runs normalisation on the given $raw input array.
	 * @param ArgparseDefinition $definition The expected input definition
	 * @param array $raw The raw command line options
	 */
	public function __construct(ArgparseDefinition $definition, array $raw) {
		$this->definition = $definition;
		$this->args = ArgsNormaliser::normalise($raw);
	}

	/**
	 * if the parsing is done, an exception can be thrown by one of the parser methods.
	 * @param bool $ignoreInvalid Boolean flag to allow for partial parsing of options
	 * @return array associative with the parsed command line options
	 */
	public function parse(bool $ignoreInvalid = false): array {
		$parsedOption = null;
		foreach ($this->args as $token) {
			//check if we are talking an option or an argument/value
			if ($token[0] !== '-') {
				//are we still adding values to an option
				if ($parsedOption !== null) {
					$parsedOption->store($token);
					//check if it's satisfied now
					if (!$parsedOption->wantMore()) {
						$parsedOption = null;
					}
				} else {
					$this->definition->storeArgumentValue($token);
				}
			} else {
				//we need to find a new option matching
				$parsedOption = $this->definition->findMatchingOption($token);

				//we didn't find a matching option
				if ($parsedOption === null) {
					if ($ignoreInvalid) {
						continue;
					}

					throw new InvalidUsageException("Unknown option '{$token}'");
				}

				//check if it even wants a value, else just call the store method once
				if ($parsedOption->nargs === Argument::NARGS_NONE) {
					$parsedOption->store('');
					$parsedOption = null;
				}
			}
		}

		$parsed = array();
		foreach (array_merge($this->definition->options, $this->definition->positionals) as $opt) {
			$opt->extract($parsed);
		}

		return $parsed;
	}
}
