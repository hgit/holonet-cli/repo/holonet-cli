<?php
/**
 * This file is part of the holonet cli package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\cli\argparse;

use InvalidArgumentException;
use holonet\cli\error\InvalidUsageException;

/**
 * the Argument class represents an option given to the script
 * this implementation is heavily influenced by python's argparse.
 */
abstract class Argument {
	/**
	 * class constant defining a value of remainder arguments given
	 * reads up to the next option for options and the remaining positional args.
	 * @var int NARGS_NONE Constant value for no arguments expected
	 */
	public const NARGS_ARRAY = -1;

	/**
	 * class constant defining a value of no arguments given.
	 * @var int NARGS_NONE Constant value for no value expected
	 */
	public const NARGS_NONE = 0;

	/**
	 * class constant defining a value of exactly 1 value for the argument given.
	 * @var int NARGS_ONE Constant value for one value expected
	 */
	public const NARGS_ONE = 1;

	/**
	 * @var mixed $default Given default value if applicable
	 */
	public $default;

	/**
	 * The description for this argument. will be used in the generated help output.
	 */
	public string $description;

	/**
	 * this is used in the usage string as a placeholder for the value.
	 */
	public ?string $metavar;

	/**
	 * The name identifier for this argument (the key in the array return value from the parser).
	 */
	public string $name;

	/**
	 * contains a nargs definition (one of the constants in this class)
	 * can also be an absolute number or -1 for remainder.
	 */
	public int $nargs;

	public bool $optional = false;

	public array $values = array();

	/**
	 * nargs can be used the following ways:
	 *  - as an integer specifying an exact number of arguments
	 *  - with -1 for reading the remainder
	 *  - with 0 for no arguments at all.
	 * @throws InvalidArgumentException if an empty name was given
	 */
	public function __construct(string $name, string $desc, int $nargs, string $metavar = null) {
		if ($name === '') {
			throw new InvalidArgumentException('Cannot create argument with empty name');
		}

		$this->name = $name;
		$this->description = $desc;
		$this->nargs($nargs);

		//if no metavar was given and we need one, we just use the name
		if ($metavar === null && $nargs !== 0) {
			$metavar = $name;
		}
		$this->metavar = $metavar;
	}

	public function default($default): self {
		$this->default = $default;

		return $this;
	}

	/**
	 * sets the internal value cache to null after saving the value.
	 * @param array $array Array with the parsed values by reference
	 * @throws InvalidUsageException if an option was not satisfied or is unknown
	 */
	public function extract(array &$array): void {
		//check if we want at least 1 value
		if (!$this->optional && $this->nargs !== 0 && $this->default === null) {
			if ($this->nargs === static::NARGS_ONE) {
				if (empty($this->values)) {
					throw new InvalidUsageException("Invalid or missing value for parameter '{$this->name}'");
				}
			} else {
				if (count($this->values) < 1) {
					throw new InvalidUsageException("Parameter '{$this->name}' expects at least one value");
				}

				if ($this->nargs !== static::NARGS_ARRAY && (count($this->values) !== $this->nargs)) {
					throw new InvalidUsageException("Parameter '{$this->name}' expects exactly {$this->nargs} values");
				}
			}
		}

		//if we got up to here it's either optional, valid or has a default
		if (!empty($this->values)) {
			if ($this->nargs === static::NARGS_ONE) {
				$array[$this->name] = array_shift($this->values);
			} else {
				$array[$this->name] = $this->values;
			}
		} elseif ($this->default !== null) {
			$array[$this->name] = $this->default;
		}

		//reset the internal value cache to allow for reparsing
		$this->values = array();
	}

	public function metavar(string $metavar): self {
		$this->metavar = $metavar;

		return $this;
	}

	public function nargs(int $nargs): self {
		if ($nargs < 0) {
			$nargs = -1;
		}
		$this->nargs = $nargs;

		return $this;
	}

	public function optional(bool $optional = true): self {
		$this->optional = $optional;

		return $this;
	}

	/**
	 * Store the string value given based on the implementation of extending classes.
	 */
	public function store(string $value): void {
		$this->values[] = $value;
	}

	/**
	 * @return bool true or false if the definition has been satisfied with enough values or not
	 */
	public function wantMore(): bool {
		switch ($this->nargs) {
			case static::NARGS_ARRAY:
				return true;
			case static::NARGS_NONE:
				return false;
			case static::NARGS_ONE:
				return empty($this->values);
			default:
				return count($this->values) !== $this->nargs;
		}
	}
}
