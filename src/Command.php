<?php
/**
 * This file is part of the holonet cli package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\cli;

use holonet\cli\io\InputDevice;
use holonet\cli\io\OutputDevice;
use holonet\cli\argparse\ArgparseDefinition;

/**
 * Command class representing a single "operation" mode inside a cli executable.
 */
abstract class Command {
	protected ArgparseDefinition $argumentDefinition;

	protected Application $cliapp;

	protected InputDevice $input;

	protected OutputDevice $output;

	/**
	 * method called in the child class to configure the command instance
	 * usually that means the option parser will be configured.
	 */
	abstract public function configure(): void;

	/**
	 * @return string with command description
	 */
	abstract public function describe(): string;

	/**
	 * method called in the child class to actually start the command process
	 * the calling code should actually run the command.
	 */
	abstract public function execute(): void;

	/**
	 * @return string with the commands's name
	 */
	abstract public function name(): string;

	/**
	 * method called to hand over references to import objects:
	 *  - Input and Output Devices
	 *  - Argument Input definition.
	 *  - Cli application instance that the command belongs to.
	 */
	public function setContext(InputDevice $in, OutputDevice $out, ArgparseDefinition $argdef, Application $cliapp): void {
		$this->input = $in;
		$this->output = $out;
		$this->argumentDefinition = $argdef;
		$this->cliapp = $cliapp;
	}
}
