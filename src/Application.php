<?php
/**
 * This file is part of the holonet cli package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\cli;

use LogicException;
use RuntimeException;
use holonet\cli\io\InputDevice;
use holonet\cli\io\OutputDevice;
use holonet\cli\io\TerminalInput;
use holonet\cli\io\TerminalOutput;
use holonet\cli\argparse\ArgparseDefinition;
use holonet\cli\error\InvalidUsageException;
use holonet\common\error\BadEnvironmentException;

/**
 * The abstract Application class should be extended in order to create a cli application.
 */
abstract class Application {
	/**
	 * @var bool ADD_META_FLAGS Constant signaling that we need meta flags (--version / --help)
	 */
	protected const ADD_META_FLAGS = true;

	/**
	 * contains an array of available commands that are loaded in by the implementing class.
	 * @var Command[] $commands Array with loaded available commands, indexed by name
	 */
	public array $commands;

	protected ArgparseDefinition $argumentDefinition;

	protected ?Command $cmd2run;

	protected InputDevice $input;

	protected OutputDevice $output;

	/**
	 * Will load in the commands, configure the options parser and print the help if needed.
	 * @param InputDevice $in Opened InputDevice to be used
	 * @param OutputDevice $out Opened OutputDevice to be used
	 * @param ArgparseDefinition $argparse Definition object for command line arguments the application expects
	 */
	public function __construct(InputDevice $in, OutputDevice $out, ArgparseDefinition $argparse) {
		$this->input = $in;
		$this->output = $out;
		$this->argumentDefinition = $argparse;

		cli_set_process_title($this->name());

		//make sure php doesn't log fatal errors twice
		ini_set('log_errors', '1');
		ini_set('display_errors', '0');

		//first, load in all available commands
		$this->loadCommands();

		if (static::ADD_META_FLAGS) {
			//initialise the option parser
			$this->argumentDefinition->addFlag('help|h', 'help', 'Display this help text');
			$this->argumentDefinition->addFlag('version|V', 'version', 'Display version information');
		}

		try {
			$this->configure();
		} catch (BadEnvironmentException $e) {
			$this->output->writeErrLn("BadEnvironmentException: {$e->getMessage()}");
			$this->exit(1);
		}

		if (empty($this->commands)) {
			$this->output->writeErrLn('Cannot run a cli application with no commands');
			$this->exit(1);
		} elseif (count($this->commands) > 1) {
			//add a global help command
			$this->addCommand(new commands\HelpCommand($this));
			//add a parsing argument for the command
			$this->argumentDefinition->addArgument('command', 'Command to be executed')->default('help')->metavar('cmd');
		}
	}

	/**
	 * @return string with the application's description
	 */
	abstract public function describe(): string;

	/**
	 * @return string with the application's name
	 */
	abstract public function name(): string;

	/**
	 * method used to run the application
	 * if only one command is added, it is executed without further checking
	 * (that also means no global help is available).
	 */
	public function run(): void {
		try {
			$this->input->setArgumentDefinition($this->argumentDefinition, true);
		} catch (InvalidUsageException $e) {
			$this->output->writeErrLn($e->getMessage());
			$this->showUsage();
			$this->exit(1);
		}

		if (count($this->commands) === 1) {
			$this->runCommand(array_shift($this->commands));
		} else {
			$command = $this->input->getArg('command');

			if (!isset($this->commands[$command])) {
				$this->output->writeErrLn("Unknown command '{$command}'. See '{$this->scriptName()} help'\n");
				$this->exit(127);
			} else {
				$this->runCommand($this->commands[$command]);
			}
		}
	}

	/**
	 * context instantiator used if the command is started from the command line
	 * opens stdin for reading and stdout/stderr for writing.
	 * @return Application instance with open command line input/output
	 */
	public static function runFromTerminal(): self {
		return new static(
			new TerminalInput(),
			new TerminalOutput(),
			new ArgparseDefinition()
		);
	}

	/**
	 * small helper method returning the name of the currently run script for the usage output
	 * this method only exists so an application could overwrite it.
	 * @return string script name to be included in the usage output
	 */
	public function scriptName(): string {
		return basename($_SERVER['PHP_SELF']);
	}

	/**
	 * @return string with version information about the application
	 */
	abstract public function version(): string;

	/**
	 * saves the command under it's named index in the commands property array.
	 * @param Command $cmd Object of class Command to be added
	 * @throws LogicException if a command with the same name was added already
	 */
	protected function addCommand(Command $cmd): void {
		$name = $cmd->name();
		if (isset($this->commands[$name])) {
			throw new LogicException("Cannot add two commands with the name '{$name}'");
		}
		$this->commands[$name] = $cmd;
	}

	/**
	 * method called in the child class to configure the Application instance
	 * usually that means the option parser will be configured.
	 */
	abstract protected function configure(): void;

	protected function exit(int $exitcode = 1): void {
		exit($exitcode);
	}

	abstract protected function loadCommands(): void;

	/**
	 * method used to run the requested command as soon as it was selected
	 * parses the parameter options defined by the Application child class
	 * as well as the ones defined by the Command class.
	 * @param Command $cmd2run The command to be run
	 */
	protected function runCommand(Command $cmd2run): void {
		$this->cmd2run = $cmd2run;

		//hand over references of everything important to the command class
		$this->cmd2run->setContext($this->input, $this->output, $this->argumentDefinition, $this);
		//add the command specific argument options
		$this->cmd2run->configure();

		try {
			$this->input->setArgumentDefinition($this->argumentDefinition);
		} catch (InvalidUsageException $e) {
			$this->output->writeErrLn($e->getMessage());
			$this->showUsage();
			$this->exit(1);
		}

		if (static::ADD_META_FLAGS) {
			if ($this->input->getArg('help') === true) {
				$this->showUsage(true);
				$this->exit(0);
			} elseif ($this->input->getArg('version') === true) {
				$this->output->writeOut(sprintf("%s Version %s\n%s\n",
					$this->name(),
					$this->version(),
					$this->describe()
				));
				$this->exit(0);
			}
		}

		try {
			//check for a before hook
			if (method_exists($this, 'beforeRun')) {
				$this->beforeRun();
			}

			//execute the command
			$this->cmd2run->execute();
			//check for an after hook
			if (method_exists($this, 'afterRun')) {
				$this->afterRun();
			}
		} catch (InvalidUsageException $e) {
			$this->output->writeErrLn("\n{$e->getMessage()}\n");
			$this->showUsage();
			$this->exit(1);
		} catch (RuntimeException $e) {
			$this->output->writeErrLn("\n{$e->getMessage()}");
			$this->output->writeErr($e->getTraceAsString());
			$this->exit(1);
		}
	}

	/**
	 * write a help usage text to stdout.
	 * @param bool $includeDescription Flag whether to include the description or not
	 */
	protected function showUsage(bool $includeDescription = false): void {
		$this->output->writeOut($this->usageText($includeDescription));
	}

	/**
	 * generate a help usage text.
	 * @param bool $includeDescription Flag whether to include the description or not
	 */
	protected function usageText(bool $includeDescription = false): string {
		$usage = '';
		if ($includeDescription) {
			$usage .= sprintf("\n%s\n%s\n\n",
				$this->describe(), //our description
				($this->cmd2run !== null ? $this->cmd2run->describe() : ''), //command's description
			);
		}

		return sprintf("%sUsage: %s %s%s\n",
			$usage,
			$this->scriptName(), //script name
			//if there are many subcommands, add that to the usage too
			(count($this->commands) !== 1 && $this->cmd2run !== null ? " {$this->cmd2run->name()} " : ' '),
			$this->argumentDefinition->getUsage(array('command')) //generated help string
		);
	}
}
