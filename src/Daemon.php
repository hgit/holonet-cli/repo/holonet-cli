<?php
/**
 * This file is part of the holonet cli package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\cli;

use RuntimeException;

/**
 * Daemon class forcing the child executable class to implement certain logic functions.
 */
abstract class Daemon extends Application {
	/**
	 * @var string $pidfile The path to the pid file of the daemon
	 */
	protected string $pidfile;

	/**
	 * method used to daemonise this process.
	 */
	protected function daemonise(): void {
		umask(0);
		$pid = pcntl_fork();
		if ($pid < 0) {
			throw new RuntimeException('Could not fork process', 100);
		}
		if ($pid > 0) {
			$this->output->writeOutLn('Sucessfully forked into daemon process');
			file_put_contents($this->pidfile, $pid);
		}

		// (pid = 0) child process

		$sid = posix_setsid(); // § 3
		if ($sid < 0) {
			exit(2);
		}

		chdir('/'); // § 4
		file_put_contents($this->pidfile, getmypid()); // § 6

//		run_process();	// cycle start data

		die('need to do more');

		if ($pid < 0) {
			echo 'fork failed';
			exit(1);
		}
	}

	/**
	 * method called to load in all available commands in the application.
	 */
	protected function loadCommands(): void {
		$this->addCommand(new commands\DaemonControlCommand());
	}

	/**
	 * abstract method called in the child class to sleep after every tick.
	 */
	abstract protected function sleep(): void;

	/**
	 * abstract method called in the child class every tick to execute the action.
	 */
	abstract protected function tick(): void;
}
