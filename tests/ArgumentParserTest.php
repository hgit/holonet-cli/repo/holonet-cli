<?php
/**
 * This file is part of the holonet cli package
 * (c) Matthias Lantsch.
 *
 * class file for test class ArgumentParserTest
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\cli\tests;

use RuntimeException;
use PHPUnit\Framework\TestCase;
use holonet\cli\argparse\ArgsNormaliser;
use holonet\cli\argparse\ArgumentParser;
use holonet\cli\argparse\ArgparseDefinition;

/**
 * The ArgumentParserTest is a test class to automatically test all the functionality of the ArgumentParser class.
 *
 * @internal
 *
 * @small
 * @coversNothing
 */
class ArgumentParserTest extends TestCase {
	/**
	 * @covers \ArgparseDefinition::addArgument()
	 * @covers \ArgumentParser::parse()
	 * @uses   \ArgumentParser
	 * @uses   \ArgparseDefinition
	 */
	public function testAddArgument(): void {
		$argv = array('testlicool');
		$def = new ArgparseDefinition();
		$def->addArgument('action', 'action parameter');
		$parser = new ArgumentParser($def, $argv);
		$results = $parser->parse();

		static::assertSame(array('action' => 'testlicool'), $results);
	}

	/**
	 * @covers \ArgparseDefinition::addFlag()
	 * @covers \ArgumentParser::parse()
	 * @uses   \ArgumentParser
	 * @uses   \ArgparseDefinition
	 */
	public function testCumulativeFlag(): void {
		$argv = array('-vvv');
		$def = new ArgparseDefinition();
		$def->addFlag('v|verbose', 'verboseness', 'Verboseness level', true);

		$parser = new ArgumentParser($def, $argv);
		$results = $parser->parse();

		static::assertSame(array('verboseness' => 3), $results);
	}

	/**
	 * @covers \ArgsNormaliser::__construct()
	 * @covers \ArgsNormaliser::expandOption()
	 * @covers \ArgsNormaliser::explodeKeyVal()
	 * @uses   \ArgsNormaliser
	 */
	public function testNormaliseValues(): void {
		$argv = array('-d=test', '--directory:test with spaces', '-fg=value of g');
		$expected = array('-d', 'test', '--directory', 'test with spaces', '-f', '-g', 'value of g');

		static::assertSame($expected, ArgsNormaliser::normalise($argv));
	}

	/**
	 * @covers \ArgparseDefinition::addOption()
	 * @covers \ArgumentParser::parse()
	 * @uses   \ArgumentParser
	 * @uses   \ArgparseDefinition
	 */
	public function testNormalLongFlag(): void {
		$argv = array('--file', 'val-f');
		$def = new ArgparseDefinition();
		$def->addOption('f|file', 'filename', 'filename');

		$parser = new ArgumentParser($def, $argv);
		$results = $parser->parse();

		static::assertSame(array('filename' => 'val-f'), $results);
	}

	/**
	 * @covers \ArgparseDefinition::addOption()
	 * @covers \ArgumentParser::parse()
	 * @uses   \ArgumentParser
	 * @uses   \ArgparseDefinition
	 */
	public function testNormalShortFlag(): void {
		$argv = array('-f', 'val-f', 'testlicool');
		$def = new ArgparseDefinition();
		$def->addOption('f|file', 'filename', 'Filename');

		$parser = new ArgumentParser($def, $argv);
		$results = $parser->parse();

		static::assertSame(array('filename' => 'val-f'), $results);
	}

	/**
	 * @covers \ArgparseDefinition::addOption()
	 * @covers \ArgumentParser::parse()
	 * @uses   \ArgumentParser
	 * @uses   \ArgparseDefinition
	 */
	public function testRequiredException(): void {
		$this->expectException(RuntimeException::class);
		$this->expectExceptionMessage("Invalid or missing value for parameter 'filename'");

		$def = new ArgparseDefinition();
		$def->addOption('f|file', 'filename', 'filename');
		$parser = new ArgumentParser($def, array());
		$parser->parse();
	}

	/**
	 * @covers \ArgparseDefinition::addArgument()
	 * @covers \ArgparseDefinition::addFlag()
	 * @covers \ArgparseDefinition::addOption()
	 * @covers \ArgumentParser::parse()
	 * @uses   \ArgumentParser
	 * @uses   \ArgparseDefinition
	 */
	public function testTokenInbetweenFlags(): void {
		$argv = array('-f', 'val-f', 'testlicool', '-g');
		$def = new ArgparseDefinition();
		$def->addOption('f|file', 'filename', 'filename');
		$def->addFlag('g|gern', 'gernval', 'gernval');
		$def->addArgument('action', 'action parameter');

		$parser = new ArgumentParser($def, $argv);

		$results = $parser->parse();

		static::assertSame(array('filename' => 'val-f', 'gernval' => true, 'action' => 'testlicool'), $results);
	}
}
